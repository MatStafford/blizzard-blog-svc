//By using "require" we tell node.js that it has to be pulled into our code because we need it.
var express = require('express');
//This Express-Object is essential and used to set up the application itself, add routes and rest-endpoints initialize middleware.
var app = express();
// This method tells the server to listen for incoming requests on the given port
app.listen(8000, function () {
    console.log('Server started!');
});
app.get('/', function (req, res) {
    res.send('Look WORDS!');
});
// app.route('/api/cats/:name').get((req, res) => {
//     const requestedCatName = req.params['name'];
//     res.send({ name: requestedCatName });
// });
// This parses the body of the requests and adds it as a new property to the request object
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.route('/api/cats').post(function (req, res) {
    res.send(201, req.body);
});
app.route('/api/cats/:name').put(function (req, res) {
    res.send(200, req.body);
});
app.route('/api/cats/:name').delete(function (req, res) {
    res.sendStatus(204);
});
// handle CORS
var cors = require('cors');
var corsOptions = {
    origin: 'http://example.com',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));
